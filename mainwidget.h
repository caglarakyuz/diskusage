#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QTreeWidgetItem>

namespace Ui {
class MainWidget;
}

class DirInfo;
class QTreeWidgetItem;

class MainWidget : public QWidget
{
	Q_OBJECT

public:
	explicit MainWidget(QWidget *parent = 0);
	~MainWidget();

protected:
	void updateTree(DirInfo *it);

protected slots:
	void timeout();

private:
	Ui::MainWidget *ui;
	DirInfo *root;
	QHash<DirInfo *, QTreeWidgetItem *> itemMap;
};

#endif // MAINWIDGET_H
