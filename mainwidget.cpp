#include "mainwidget.h"
#include "ui_mainwidget.h"

#include <QDir>
#include <QPair>
#include <QDebug>
#include <QTimer>
#include <QMutex>
#include <QThread>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

class DirInfo {
public:
	DirInfo(const QString &p, __dev_t rdev)
	{
		path = p;
		rootDev = rdev;
		size0 = 0;
		size1 = 0;
	}

	QString getPath()
	{
		return path;
	}

	void listChildren()
	{
		stats.addDir();
		QDir d(path);
		/* list and recurse into directories */
		QStringList dirs = d.entryList(QDir::NoDotAndDotDot | QDir::Dirs | QDir::NoSymLinks | QDir::Hidden);
		foreach (const QString &dir, dirs) {
			QString abspath = d.absoluteFilePath(dir);
			struct stat sbuf;
			stat(qPrintable(abspath), &sbuf);
			if (rootDev != sbuf.st_dev)
				continue;
			DirInfo *sub = new DirInfo(abspath, rootDev);
			mutex.lock();
			children << sub;
			mutex.unlock();
		}
	}

	void digChildren()
	{
		foreach (DirInfo *info, children)
			info->listChildren();
		foreach (DirInfo *info, children)
			info->digChildren();
	}

	qint64 findDirSize()
	{
		size0 = 0;
		size1 = 0;
		QDir d(path);
		/* list and count files */
		QStringList files = d.entryList(QDir::Files | QDir::NoSymLinks | QDir::Hidden);
		foreach (const QString &file, files) {
			QString abspath = d.absoluteFilePath(file);
			struct stat sbuf;
			stat(qPrintable(abspath), &sbuf);
			if (rootDev != sbuf.st_dev)
				continue;
			size0 += sbuf.st_size;
		}
		foreach (DirInfo *info, children)
			size1 += info->findDirSize();
		stats.addDirDone();
		return size0 + size1;
	}

	qint64 size()
	{
		return size0 + size1;
	}

	QList<DirInfo *> getChildren()
	{
		QMutexLocker ml(&mutex);
		return children;
	}

	class DUStats {
	public:
		DUStats()
		{
			dirCount = 0;
			fileCount = 0;
			dirDoneCount = 0;
		}

		void addDir()
		{
			dirCount++;
		}

		void addFile()
		{
			fileCount++;
		}

		void addDirDone()
		{
			dirDoneCount++;
		}

		qint64 getDirCount()
		{
			return dirCount;
		}

		qint64 getDirDoneCount()
		{
			return dirDoneCount;
		}

	protected:
		qint64 dirDoneCount;
		qint64 dirCount;
		qint64 fileCount;
	};
	static DUStats stats;
protected:
	__dev_t rootDev;
	QString path;
	QList<DirInfo *> children;
	qint64 size0;
	qint64 size1;
	QMutex mutex;
};

DirInfo::DUStats DirInfo::stats;

class DUInfo {
public:

	static DUInfo * instance()
	{
		static DUInfo *inst = NULL;
		if (!inst)
			inst = new DUInfo;
		return inst;
	}

	void addDir(const QString &path, qint64 size)
	{
		total += size;

		if (size > 1024 * 1024 * 100) {
			QMutexLocker ml(&l);
			bigFolders[size] << path;
		}
	}

	void incrementProgress(int count = 1)
	{
		progress += count;
	}

	void incrementTotalProgress(qint64 count)
	{
		totalProgress += count;
	}

	qint64 getTotalProgress()
	{
		return totalProgress;
	}

	qint64 getProgress()
	{
		return progress;
	}

	QMap<qint64, QStringList> getBigFolders()
	{
		QMutexLocker ml(&l);
		return bigFolders;
	}

protected:

	DUInfo()
	{
		total = 0;
		progress = 0;
		totalProgress = 0;
	}

	QMutex l;
	qint64 total;
	qint64 progress;
	qint64 totalProgress;
	QMap<qint64, QStringList> bigFolders;
};

static QPair<QStringList, qint64> listEntries(const QString &path, __dev_t rootDev)
{
	qint64 total = 0;

	QPair<QStringList, qint64> p;
	QDir d(path);

	/* list and recurse into directories */
	QStringList dirs = d.entryList(QDir::NoDotAndDotDot | QDir::Dirs | QDir::NoSymLinks);
	DUInfo::instance()->incrementTotalProgress(dirs.size());
	foreach (const QString &dir, dirs) {
		DUInfo::instance()->incrementProgress();
		QString abspath = d.absoluteFilePath(dir);
		struct stat sbuf;
		stat(qPrintable(abspath), &sbuf);
		if (rootDev != sbuf.st_dev)
			continue;
		p.first << abspath;
		total += listEntries(abspath, rootDev).second;
	}

	/* list and count files */
	QStringList files = d.entryList(QDir::Files | QDir::NoSymLinks);
	foreach (const QString &file, files) {
		QString abspath = d.absoluteFilePath(file);
		struct stat sbuf;
		stat(qPrintable(abspath), &sbuf);
		if (rootDev != sbuf.st_dev)
			continue;
		total += sbuf.st_size;
	}
	p.second = total;

	/* report progress */
	DUInfo::instance()->addDir(path, total);

	return p;
}

class ListThread : public QThread
{
public:
	DirInfo *root;
	void run()
	{
		root->listChildren();
		root->digChildren();
		qDebug("List thread finished");
	}
};

class DigThread : public QThread
{
public:
	DirInfo *root;
	void run()
	{
		QThread::sleep(60);
		root->findDirSize();
		qDebug("Dig thread finished");
	}
};

MainWidget::MainWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::MainWidget)
{
	ui->setupUi(this);

	struct stat sbuf;
	__dev_t rootDev;
	stat("/", &sbuf);
	rootDev = sbuf.st_dev;
	root = new DirInfo("/", rootDev);

	ListThread *lth = new ListThread;
	DigThread *dth = new DigThread;
	dth->root = root;
	lth->root = root;

	lth->start();
	dth->start();

	QTimer::singleShot(0, this, SLOT(timeout()));
}

MainWidget::~MainWidget()
{
	delete ui;
}

static QString getItemSizeString(DirInfo *item)
{
	return QString("%1 MB").arg(item->size() / 1024 / 1024);
}

void MainWidget::updateTree(DirInfo *it)
{
	QList<DirInfo *> children = it->getChildren();
	if (!itemMap.contains(it)) {
		QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget, QStringList() << it->getPath() << 0);
		itemMap.insert(it, item);
	}
	itemMap[it]->setText(1, getItemSizeString(it));
	foreach (DirInfo *child, children) {
		if (!itemMap.contains(child)) {
			QTreeWidgetItem *item = new QTreeWidgetItem(itemMap[it], QStringList() << child->getPath() << 0);
			itemMap.insert(child, item);
		}
		QTreeWidgetItem *item = itemMap[child];
		item->setText(1, getItemSizeString(child));
		/* update if parent is expanded */
		if (itemMap[it]->isExpanded())
			updateTree(child);

	}
}

void MainWidget::timeout()
{
#if 0
	DUInfo *info = DUInfo::instance();
	ui->progressBar->setMaximum(info->getTotalProgress());
	ui->progressBar->setValue(info->getProgress());
	QMap<qint64, QStringList> bf = info->getBigFolders();
	if (ui->listWidget->count() != bf.size()) {
		ui->listWidget->clear();
		QMapIterator<qint64, QStringList> mi(bf);
		while (mi.hasNext()) {
			mi.next();
			QStringList paths = mi.value();
			foreach (QString path, paths)
				ui->listWidget->addItem(QString("%1 - %2").arg(path).arg(mi.key() / 1024 / 1024));
		}
	}
#else
	qDebug() << DirInfo::stats.getDirCount() << DirInfo::stats.getDirDoneCount();
	ui->progressBar->setMaximum(DirInfo::stats.getDirCount());
	ui->progressBar->setValue(DirInfo::stats.getDirDoneCount());
	foreach (DirInfo *child, root->getChildren())
		updateTree(child);
#endif

	QTimer::singleShot(1000, this, SLOT(timeout()));
}
